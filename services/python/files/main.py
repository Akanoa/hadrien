import base64
import io
import secrets

from datetime import datetime, timedelta

from fastapi import FastAPI, Response, status, Cookie
from pydantic import BaseModel

from neo4j import GraphDatabase, Result
from neo4j import Transaction
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA512
from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.Random import get_random_bytes
from starlette.middleware.cors import CORSMiddleware

from jose import JWTError, jwt

app = FastAPI()
driver = GraphDatabase.driver("bolt://neo4j:7687", auth=("neo4j", "test"))
key = get_random_bytes(16)

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 10

origins = [
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def create_access_token(data: dict, expires_data: timedelta):
    """
    Create a JWT string from data
    :param data: Data to encode
    :param expires_data:
    :return: The token as string
    """
    to_encode = data.copy()
    expire_time = datetime.utcnow() + expires_data
    to_encode.update({"exp": expire_time})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


class ChallengeRequest(BaseModel):
    public_key: str

    def __str__(self):
        return self.public_key


class ChallengeAnswer(BaseModel):
    answer: str


class Token(BaseModel):
    access_token: str
    token_type: str


def is_public_key_referred(public_key: str):
    public_key = public_key.split('\n')[1]

    with driver.session() as session:
        result = session.run("MATCH (p: Peer) WHERE p.public_key = $public_key RETURN p.root", public_key=public_key)
        return not (result.single() is None)


@app.post("/ask_for_challenge")
def ask_for_challenge(challenge_demand: ChallengeRequest, response: Response):
    if not is_public_key_referred(challenge_demand.public_key):
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return

    public_key = RSA.importKey(challenge_demand.public_key)
    cipher = PKCS1_OAEP.new(public_key, SHA512.new())

    tokens = secrets.token_hex(24)

    answer_cipher = AES.new(key, AES.MODE_EAX)

    challenge = bytes(tokens, "utf-8")

    challenge_encrypted = cipher.encrypt(challenge)
    answer, tag = answer_cipher.encrypt_and_digest(challenge)

    buffer = io.BytesIO()
    # nonce : 16 bits
    # MAC : 16 bits
    # answer : remaining
    [buffer.write(x) for x in (answer_cipher.nonce, tag, answer)]

    buffer.seek(0)
    base64_answer = base64.b64encode(buffer.read())

    response.set_cookie(key="answer", value=base64_answer.decode('utf-8'), path="/", secure=True, httponly=True)

    return {
        "challenge": base64.b64encode(challenge_encrypted)
    }


@app.post("/resolve_challenge")
def resolve_challenge(challenge_answer: ChallengeAnswer,
                      response: Response,
                      answer: str = Cookie(None),
                      ):
    answer_bytes = base64.b64decode(answer)
    buffer = io.BytesIO(answer_bytes)
    buffer.seek(0)
    nonce, tag, answer_string = [buffer.read(x) for x in (16, 16, -1)]
    cipher = AES.new(key, AES.MODE_EAX, nonce)
    data = cipher.decrypt_and_verify(answer_string, tag).decode('utf-8')

    response.delete_cookie(key="answer")

    if data != challenge_answer.answer:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        response.delete_cookie(key="token_jwt")
        return

    token = create_access_token({}, timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES))
    response.set_cookie(key="token_jwt", value=token, path="/", secure=True, httponly=True)


@app.get("/private")
def private(response: Response, token_jwt: str = Cookie(None)):
    if token_jwt is None:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return

    try:
        jwt.decode(token_jwt, SECRET_KEY, algorithms=ALGORITHM)
    except JWTError:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return

    return {
        "message": "success"
    }
